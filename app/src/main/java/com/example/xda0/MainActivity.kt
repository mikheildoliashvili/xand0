package com.example.xda0

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.util.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var button1:Button
    private lateinit var button2:Button
    private lateinit var button3:Button
    private lateinit var button4:Button
    private lateinit var button5:Button
    private lateinit var button6:Button
    private lateinit var button7:Button
    private lateinit var button8:Button
    private lateinit var button9:Button
    private lateinit var resetbutton: Button
    private lateinit var pirveliMotamashe: TextView
    private lateinit var meoreMotamashe: TextView
    private var activePlayer = 1
    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()
    private var emptyCells = ArrayList<Int>()
    private var player1Count = 0
    private var player2Count = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        resetbutton.setOnClickListener(){
            reset()
        }
    }



    private fun init(){
        pirveliMotamashe= findViewById(R.id.pirveli)
        meoreMotamashe= findViewById(R.id.meore)
        resetbutton=findViewById(R.id.btnReset)
        button1= findViewById(R.id.buttonid1)
        button2= findViewById(R.id.buttonid2)
        button3= findViewById(R.id.buttonid3)
        button4= findViewById(R.id.buttonid4)
        button5= findViewById(R.id.buttonid5)
        button6= findViewById(R.id.buttonid6)
        button7= findViewById(R.id.buttonid7)
        button8= findViewById(R.id.buttonid8)
        button9= findViewById(R.id.buttonid9)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

    }



    override fun onClick(clickedView: View?) {
        if (clickedView is View){
            var buttonNumber = 0
            when (clickedView.id) {
                R.id.buttonid1 -> buttonNumber = 1
                R.id.buttonid2 -> buttonNumber = 2
                R.id.buttonid3 -> buttonNumber = 3
                R.id.buttonid4 -> buttonNumber = 4
                R.id.buttonid5 -> buttonNumber = 5
                R.id.buttonid6 -> buttonNumber = 6
                R.id.buttonid7 -> buttonNumber = 7
                R.id.buttonid8 -> buttonNumber = 8
                R.id.buttonid9 -> buttonNumber = 9
            }
            if (buttonNumber!=0){
                playGame(clickedView as Button, buttonNumber)
            }
        }
    }
    fun reset() {
        firstPlayer.clear()
        secondPlayer.clear()
        emptyCells.clear()
        activePlayer = 1;
        for(i in 1..9) {
            var buttonselected : Button?
            buttonselected = when(i){
                1 -> button1
                2 -> button2
                3 -> button3
                4 -> button4
                5 -> button5
                6 -> button6
                7 -> button7
                8 -> button8
                9 -> button9
                else -> {resetbutton}
            }
            buttonselected.isEnabled = true
            buttonselected.text = ""
            pirveliMotamashe.text = "Player1 : $player1Count"
            meoreMotamashe.text = "Player2 : $player2Count"
        }
    }

    private fun playGame(clickedView: Button, buttonNumber: Int) {
        if (activePlayer==1){
            clickedView.text = "X"
            clickedView.setBackgroundColor(Color.RED)
            activePlayer = 2
            firstPlayer.add(buttonNumber)
        }else {
            secondPlayer.add(buttonNumber)
            clickedView.text = "0"
            clickedView.setBackgroundColor(Color.YELLOW)
            activePlayer=1

        }
        clickedView.isEnabled = false
        check()
    }
    private fun check(){
        var winnerPlayer = 0
        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(4) && secondPlayer.contains(5) && secondPlayer.contains(6)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(7) && secondPlayer.contains(8) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(4) && secondPlayer.contains(7)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(2) && secondPlayer.contains(5) && secondPlayer.contains(8)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(6) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(5) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(5) && firstPlayer.contains(7)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(5) && secondPlayer.contains(7)){
            winnerPlayer = 2
        }
        if (winnerPlayer == 1)
            Toast.makeText(this , "first player won" , Toast.LENGTH_LONG).show()
        else if (winnerPlayer == 2)
            Toast.makeText(this , "second player won" , Toast.LENGTH_LONG).show()

    }





}